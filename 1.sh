#!/bin/bash
sudo apt-get --quiet update --yes
sudo apt-get --quiet install --yes wget tar unzip lib32stdc++6 lib32z1 git sudo curl
git config --global user.name "Blue Dogerino"
git config --global user.email "bluedogerino@gmail.com"
git clone https://github.com/akhilnarang/scripts
cd scripts
bash setup/android_build_env.sh
cd ..
mkdir ~/bin
export PATH=~/bin:$PATH
curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
mkdir ~/lineage
cd ~/lineage
repo init -u https://github.com/LineageOS/android.git -b lineage-15.1
mkdir -p .repo/local_manifests
cd .repo/local_manifests
wget https://gitlab.com/bluedoge-j5y17lte/local_manifest/raw/master/roomservice.xml
cd ../..
repo sync -f --force-sync --no-clone-bundle -j64 --quiet
. build/envsetup.sh
brunch j5y17lte